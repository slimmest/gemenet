# GeMeNet ~ Genomes to Metabolic Networks


GeMeNet is a pipeline for generating multiple metabolic networks essentially from their genomes but from other data (gbk, ect...).

## GeMeNet: Pipeline summary
![alt text](img/resume_steps_gemenet.png)
 
## Table of Contents

- [Installation](#Installation)
    - [Conda environment](#Conda)
    - [Path singularity image of pathwaytools](#singularity)
    - [Download eggnog database](#database)
    - [Download GO ontology database](#databasego)
    - [Specific requirements](#requirements)
- [Data requirements](#data)
    - [Input data](#Lineages)
    - [Lineage files](#inputs)
    - [Output data](#Outputs)
- [Usages](#usages)
    - [all mode](#all)
    - [em mode](#em)
    - [gbk mode](#gbk)
    - [pw mode](#pw)
- [HPC specificities](#HPC)
  - [Gemenet conda activation](#Gemenet)
  - [Arguments #SBATCH (Arrays and Time)](#SBATCH)




-----------------------------------
# General Installation: <a name="Installation"></a>


### Step 1 :Conda environment <a name="Conda"></a>
Create the environnement GeMeNet.yml: 

```
conda env create -f gemenet.yml
```

### Step 2 : Add path singularity image of pathwaytools: <a name="singularity"></a>
- **Pathwaytools:** 

For HPC, you must install pathwaytools, refer to this [link](https://metage2metabo.readthedocs.io/en/latest/install.html ).


- **Sigularity image :** 

Before launching the pipeline you must define in the script `4_mpwt.sh` line 60, the path of **your** singularity image of pathwaytools, then it can be used for the mpwt step:

```
srun singularity exec -B <PATH>/:<PATH> <PATH>/mpwt_070_pwtools_25_5.sif bash 4_recon.sh -i ${gbk_dir} -p ${path}
```

###  Step 3 :Download eggnog database: <a name="database"></a>
The thrid step is to download the eggnog database. The script `download_eggnog_data.py` is to be found in [the eggNOG-mapper github ](https://github.com/eggnogdb/eggnog-mapper/wiki/eggNOG-mapper-v2.1.5-to-v2.1.8 )

Before running the script you have to create the folder `data/` in the folder site-packages of the library python3.10 in your gemenet environment.
After the folder is created you can run the script download_eggnog_data.py (with the gemenet environment enabled) and accept to download the databases.

**/!\ Be careful: the databases are very heavy and you must plan to have enough space to store them.**

```
conda activate gemenet
cd <PATH>/miniconda3/envs/gemenet/lib/python3.10/site-packages/
mkdir data
cd -
python download_eggnog_data.py
```

###  Step 4 :Download GO onthology database: <a name="databasego"></a>
The last step is to download the GO onthology database. You must download the Go onthology and give it as an input path to the program with the argument `-b`.


You can download it here: http://purl.obolibrary.org/obo/go/go-basic.obo . or use the version on the git in the "Go_basic" directory.


-----------------------------------
# Global tools: <a name="Installation"></a>

**[Prodigal V2.6.3](https://github.com/hyattpd/Prodigal)** is a protein-coding gene prediction software tool for bacterial and archaeal genomes.
It take in input a nucleic sequence fasta file (genome file) and by prediction creates :

- a protein translations file (proteome_file.faa)
- a new nucleotide fasta file with annotation (CDS sequence) (genome_file.fna)


**[EggNOG-mapper 2.1.9](https://github.com/eggnogdb/eggnog-mapper)** is a tool for fast functional annotation of novel sequences. It generate an annotation files xxx.emapper.annotation from proteome files (faa).

**/!\ NB:** the extension xxx.emapper.annotation will be convert in tsv extension for the next step.

**[emapper2gbk 0.3.0](https://github.com/AuReMe/emapper2gbk)** is a tool to build gbk files that are suitable for metabolic network reconstruction with Pathway Tools from genome files, proteome files and annotated fasta and Eggnog-mapper annotation files.

**/!\ NB:** For this step, it mandatory to provide a tsv file with lineage of each genome. Example in the section [Lineage files](#inputs)

**[MPWT 0.7.0 (with pathwaytools 26.5)](https://github.com/AuReMe/mpwt)** is a python package for running Pathway Tools on multiple genomes and reconstruct a metabolic network for each organisms. 

**[PADMet 5.0.1](https://github.com/AuReMe/padmet)** with:
- ***1. pgdb_to_padmet*** is the step to convert .dat files from MWPT to .padmet files.
-  ***2. sbmlGenerator*** is the step to convert .padmet files file from PADMet to SMBL files 


-----------------------------------
# Data requirements <a name="data"></a>
## Input data: <a name="inputs"></a>

#### ~ The name of the files:

The file names used in input in different modes must follow certain criteria:
- The files names must be preserved throughout the pipeline, in the different input files for a given genome(genome, proteomes, gbk ect).


- eggNog_mapper does not support special characters, so the file names must not have any special characters such as: (, ), . , !, ?, [, ], #, +,: .
#### /!\ Please note that if you change the names of your inputs, you must change it in the lineage.tsv file, in the column one in order to match the names /!\

```
example:
Annotated_genome_folder_input
├── species_1.fna
├── species_4.fna
├── species_10.fna
├── ...

Proteome_folder_input
├── species_1.faa
├── species_4.faa
├── species_10.faa
├── ...

```
___________________________________

#### ~ The organisation of data according to the mode:

The organisation of the files will depend on the mode you choose, there are 4 modes depending on your input data:

##### Intermediate files are kept in other folders, either with a specified path or in the output directory by default

### - `all` mode :
This mode runs the entire pipeline and begins with as input data *unannotated genomes* and the output data are *metabolite networks*.

```
Folder_input
├── species_1.fa
├── species_4.fasta
├── species_10.fna
├── ...
```

### - `em` mode :
This mode runs a part of the pipeline and begins with as input data *annotated genomes* and *proteomes*, the output data are *metabolite networks*.

The different input folders can be grouped in a main directory or separated.

```
Annotated_genome_folder_input
├── species_1.fna
├── species_4.fna
├── species_10.fna
├── ...

Proteome_folder_input
├── species_1.faa
├── species_4.faa
├── species_10.faa
├── ...

```

#### - `gbk` mode:
This mode runs a part of the pipeline and begins with as input data *annotated genomes*, *proteomes* and **annotation files** from eggnogmapper, the output data are *metabolite networks*.

The different input folders can be grouped in a main directory or separated.
**/!\ Be careful the annotation file must be in .tsv files !**

It's possible to give for each species one .tsv file but it's also possible to give a big annotation file with annotations for all species.

```
Annotated_genome_folder_input
├── species_1.fna
├── species_4.fna
├── species_10.fna
├── ...

Proteome_folder_input
├── species_1.faa
├── species_4.faa
├── species_10.faa
├── ...


Annotation_folder_input
├── species_1.tsv
├── species_4.tsv
├── species_10.tsv
├── ...

 OR 

├── All_annotation_in_one_file.tsv

```

#### - `pw` mode
This mode runs a part of the pipeline and begins with as input data *gbk files* and the output data are *metabolite networks*.

The taxon_id.tsv file will be created automatically by the pipeline, it's not necessary to provide one.

```
gbk_folder_input
├── species_1.gbk
├── species_4.gbk
├── species_10.gbk
├── ...
```
________________________________________

## Lineage file: <a name="Lineages"></a>
The lineage file is used by the emapper2gbk tool to facilitate the automation of the taxon_id search for each species.

On the left in the column we have the names of the files we have 
In the right column we have the full taxonomy.

Then, if the strain is not found in the first query in the NCBI database, in the second query, it will search at the species level and this continues until we find a query that works.

 /!\ Be careful, it is possible that the lineage you have of your data is obsolete compared to the database, in this case the .dat files of the mpwt step will not be created

Example tsv file for lineage:
| user_genome                                      | classification                                                                                                                                          |
|--------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|
| Escherichia_coli_str_K-12_substr_MG1655_genomic  | Bacteria;Proteobacteria;Gammaproteobacteria;Enterobacteriales;Enterobacteriaceae;Escherichia;Escherichia coli;Escherichia coli str. K-12 substr. MG1655 |
| Rickettsia_prowazekii_str_Madrid_E_genomic       | Bacteria;Proteobacteria;Alphaproteobacteria;Rickettsiales;Rickettsiaceae;Rickettsia;Rickettsia prowazekii;Rickettsia prowazekii str. Madrid E           |
| Bacillus_subtilis_subsp_subtilis_str_168_genomic | Bacteria;Firmicutes;Bacilli;Bacillales;Bacillaceae;Bacillus;Bacillus subtilis;Bacillus subtilis subsp. subtilis str. 168                                |
| Deinococcus_radiodurans_R1_genomic               | Bacteria;Deinococcus-Thermus;Deinococci;Deinococcales;Deinococcaceae;Deinococcus;Deinococcus radiodurans;Deinococcus radiodurans R1                     |
| Xylella_fastidiosa_9a5c_genomic                  | Bacteria;Proteobacteria;Gammaproteobacteria;Xanthomonadales;Xanthomonadaceae;Xylella;Xylella fastidiosa;Xylella fastidiosa 9a5c                         |
| Pseudomonas_aeruginosa_PAO1_genomic              | Bacteria;Proteobacteria;Gammaproteobacteria;Pseudomonadales;Pseudomonadaceae;Pseudomonas;Pseudomonas aeruginosa;Pseudomonas aeruginosa PAO1             |
| Bacillus_halodurans_C-125_genomic                | Bacteria;Firmicutes;Bacilli;Bacillales;Bacillaceae;Bacillus;Bacillus halodurans;Bacillus halodurans C-125                                               |
______________________

## GFF files: <a name="Lineages"></a>

Gff files are optionals but can be provide for `all`,`em`, `gbk` mode.

To work the IDs of the genes in the **genome files**  must be the same than the IDs of the proteins in the **protein file**  and in the **annotation file**. For more details refer to [emapper2gbk 0.3.0](https://github.com/AuReMe/emapper2gbk).

## Output data: <a name="Outputs"></a>

#### **/!\ Be careful, depending on the mode chosen the options (`-g`, `-p`, `-a`, `-k`) will be for the input or output path of your data**

***Examples:***

With `all` mode, the argument `-i` is for genome input path and `-g`, `-p`, `-a`, `-k` are for specific outputs paths if you want one.


With `em` mode, the arguments `-g`, `-p`, are for data inputs paths and  `-a`, `-k` are for specific outputs paths if you want one.

With `gbk` mode, the arguments `-g`, `-p`,`-a`, are for data inputs paths and `-k` is for specific outputs paths if you want one.

With `pw` mode, the argument `-k` is for gbk input path and the ouputs will be define by the default ouput argument `-o`



There are two possibilities to store our data outputs:


#### **- By default:** 
In this case we do not give the options relative to these outputs (-g, -p, -a, -k) and all the sub-folders of our intermediate files will be created in the output directory that we have defined for our metabolite networks, the default output directory.

A RECAP.ME file is created in the default output directory with a recap of the tools and their version and each commands with their arguments.

#### **Output by default:**

```
Folder_output
├── Genome
│   └── species_1.fna
│   └── species_2.fna
│   └── ...
├── Proteome
│   └── species_1.faa
│   └── species_2.faa
│   └── ...
├── Annotation
│   └── species_1.tsv
│   └── species_2.tsv
│   └── ...
├── EggnoGmapper2gbk
│   └── species_1
│       └── species_1.gbk
│   └── species_2
│       └── species_2.gbk
│   └── ...
│   └── taxon_id.tsv
├── mpwt
│   └── species_1
│       └── many_files_for_species_1.dat
│   └── species_2
│       └── many_files_for_species_2.dat
│   └── ...
├── Padmet
│   └── species_1
│       └── species_1.padmet
│   └── species_2
│       └── species_2.padmet
│   └── ...
├── SMBL
│   └── species_1
│       └── species_1.sbml
│   └── species_2
│       └── species_2.sbml
│   └── ...
```


#### **Specific output:**
- In the other case, it's possible to specify a particular file for all intermediary output. Just add the relative options (-g, -p, -a, -k) and define the desired path for each.
```
Folder_output_by_default
├── SMBL
│   └── species_1
│       └── species_1.sbml
│   └── species_2
│       └── species_2.sbml
│   └── ...
├── mpwt
│   └── species_1
│       └── many_files_for_species_1.dat
│   └── species_2
│       └── many_files_for_species_2.dat
│   └── ...
├── Padmet
│   └── species_1
│       └── species_1.padmet
│   └── species_2
│       └── species_2.padmet
│   └── ...

specific_output_folder
├── Genome_directory
│   └── species_1.fna
│   └── species_2.fna
│   └── ...
├── Proteome_directory
│   └── species_1.faa
│   └── species_2.faa
│   └── ...
├── Annotation_directory
│   └── species_1.tsv
│   └── species_2.tsv
│   └── ...

specific_output_folder2
├── EggnoGmapper2gbk_directory
│   └── species_1
│       └── species_1.gbk
│   └── species_2
│       └── species_2.gbk
│   └── ...
│   └── taxon_id.tsv


```


# Usages <a name="usages"></a>

### - `all` mode <a name="all"></a>

```
usage with default outputs:
sbatch 0_GeMeNet.sh [-h] -c CHOICE -r ARRAY  [-i FASTANUCLEIC] [-o OUTPUT] [-b GO_ONTOLOGYFILE] [-t TAXONFILE]

usage with with specific outputs: 
sbatch 0_GeMeNet.sh [-h] -c CHOICE -r ARRAY [-i FASTANUCLEIC] [-o OUTPUT] [-b GO_ONTOLOGYFILE] [-t TAXONFILE] [-g FASTANUCLEIC_ANNOT] [-p FASTAPROT] [-a ANNOTATION] [-k GBK] [-f GFF] [-e PADMET_REF]

MANDATORY arguments:
  -c, CHOICE,       Choice the first step to begin depending of your inputs data                          
                      -all : from the beginning with only the genome 
                      -em : from emapper with the genomes + proteomes files
                      -gbk : from emapper2gbk with annotated genomes + proteomes files + annotations files 
                      -pw : from mpwt step with the gbk files 

  -r                 Define how many job array we want to run '--array=x-y' and we define the maximum number
                     of simultaneously running tasks from the job array by a '%' separator '--array=x-y%z'. 
                     For example: '--array=0-150%10' we will run 150 jobs array in total but we will limit the 
                     number of simultaneously running tasks from this job array to 10.
  
  -i, FASTANUCLEIC, Path for the input directory with unannotated genomes files 
                       
  -o, OUTPUT,       Directory path for metabolic networks and the output directory by default 
                    for all other steps (proteomes,annotation, gbk ...) if no path has been 
                    defined before.                 
                        
  -b  GO_ONTOLOGY   Path for the GO-ontology -go basic file                       

  -t, TAXONFILE     Path for the lineage file. It's a tsv file commposed of two columns:
                      -the first one with the name of each genome file and 
                      -the second one the complete lineage (more details here) 

OPTIONAL arguments:
  -h, --help        Show this help message and exit             

  -g, FASTA_ANNOT     Path for the directory where you want the created annotated genome files             
                        
  -p, FASTAPROT       Path for the directory where you want the created proteome directory  
                        
  -a ANNOTATION       Path for the directory where you want the created annotated tsv files 
                        
  -k GBK              Path for the directory where you want the gbk files

  -f GFF              Path for your EXISTING directory where are gff files 

  -e PADMET_REF     Path to the padmetRef if you want to use ref for the construction of the sbml
```

##### /!\ For -g, -p, -a, -k options, if you don't define the path, a folder will be created by default in the output directory you defined in option -o. 


  - Example of execution with default outputs:
```
sbatch 0_GeMeNet.sh -c all -r 0-150%10 -i fasta_dir/ -o output_dir/ -b Databases/Go_basic/go-basic.obo -t Lineages/lineage.tsv -f gff_folder/ -e Databases/Ref_Padmet/metacyc25-5.padmet
```

- Example of execution with specific outputs directory that the user wants:
```
sbatch 0_GeMeNet.sh -c all -r 0-150%10 -i fasta_dir/ -o output_dir/ -b Databases/Go_basic/go-basic.obo -t Lineages/lineage.tsv -g Genomes/ -p Proteomes_folder/ -a Annotation_folder/ -k gbk_folder/ -f gff_folder/ -e Databases/Ref_Padmet/metacyc25-5.padmet
```
____________________________________

### - `em` mode <a name="em"></a>

```
usage with default outputs:
sbatch 0_GeMeNet.sh [-h] -c CHOICE -r ARRAY [-o OUTPUT] [-b GO_ONTOLOGYFILE] [-t TAXONFILE] [-g FASTANUCLEIC_ANNOT] [-p FASTAPROT]

usage with with specific outputs: 
sbatch 0_GeMeNet.sh [-h] -c CHOICE -r ARRAY [-o OUTPUT] [-b GO_ONTOLOGYFILE] [-t TAXONFILE] [-g FASTANUCLEIC_ANNOT] [-p FASTAPROT] [-a ANNOTATION] [-k GBK] [-f GFF] [-e PADMET_REF]

Mandatory arguments:
  -c, CHOICE,       Choice the first step to begin depending of your inputs data                          
                      -all : from the beginning with only the genome 
                      -em : from emapper with the genomes + proteomes files
                      -gbk : from emapper2gbk with annotated genomes + proteomes files + annotations files 
                      -pw : from mpwt step with the gbk files 

  -r                 Define how many job array we want to run '--array=x-y' and we define the maximum number
                     of simultaneously running tasks from the job array by a '%' separator '--array=x-y%z'. 
                     For example: '--array=0-150%10' we will run 150 jobs array in total but we will limit the 
                     number of simultaneously running tasks from this job array to 10.

  -o, OUTPUT,       Directory path for metabolic networks and the output directory by default 
                    for all other steps (annotation, gbk ...) if no path has been 
                    defined before.                 
                        
  -b  GO_ONTOLOGY   Path for the GO-ontology -go basic file                       

  -t, TAXONFILE     Path for the lineage file. It's a tsv file commposed of two columns:
                      -the first one with the name of each genome file and 
                      -the second one the complete lineage (more details here) 
  
  -g, FASTA_ANNOT     Path for your EXISTING directory where are annotated genomes file     
                        
  -p, FASTAPROT       Path for your EXISTING directory where are proteome files 
                        

optional arguments:
  -h, --help        Show this help message and exit             

  -a ANNOTATION       Path for the directory where you want the created annotated tsv files 
                        
  -k GBK              Path where you want the gbk files 

  -f GFF              Path for your EXISTING directory where are gff files (not mandatory, run normally without gff)

  -e PADMET_REF     Path to the padmetRef if you want to use ref for the construction of the sbml
```

#####  /!\ For -a, -k options, if you don't define the path, a folder will be created by default in the output directory you defined in option -o.

- Example of execution with default outputs:
```
sbatch 0_GeMeNet.sh -c em -r 0-150%10 -o output_dir/ -b Databases/Go_basic/go-basic.obo -t Lineages/lineage.tsv -g Genomes/ -p Proteomes_folder/  -f gff_folder/ -e Databases/Ref_Padmet/metacyc25-5.padmet
```

- Example of execution with specific outputs directory that the user wants:
```
sbatch 0_GeMeNet.sh -c em -r 0-150%10 -o output_dir/ -b Databases/Go_basic/go-basic.obo -t Lineages/lineage.tsv -g Genomes/ -p Proteomes_folder/ -a Annotation_folder/ -k gbk_folder/ -f gff_folder/ -e Databases/Ref_Padmet/metacyc25-5.padmet
```

__________________________________________
### - `gbk` mode <a name="gbk"></a>
```
usage with default outputs:
sbatch 0_GeMeNet.sh [-h] -c CHOICE -r ARRAY [-o OUTPUT] [-b GO_ONTOLOGYFILE] [-t TAXONFILE] [-g FASTANUCLEIC_ANNOT] [-p FASTAPROT] [-a ANNOTATION] 

usage with with specific outputs: 
sbatch 0_GeMeNet.sh [-h] -c CHOICE -r ARRAY [-o OUTPUT] [-b GO_ONTOLOGYFILE] [-t TAXONFILE] [-g FASTANUCLEIC_ANNOT] [-p FASTAPROT] [-a ANNOTATION] [-k GBK] [-f GFF] [-e PADMET_REF]

                   
Mandatory arguments:
  -c, CHOICE,       Choice the first step to begin depending of your inputs data                          
                      -all : from the beginning with only the genome 
                      -em : from emapper with the genomes + proteomes files
                      -gbk : from emapper2gbk with annotated genomes + proteomes files + annotations files 
                      -pw : from mpwt step with the gbk files 

  -r                 Define how many job array we want to run '--array=x-y' and we define the maximum number
                     of simultaneously running tasks from the job array by a '%' separator '--array=x-y%z'. 
                     For example: '--array=0-150%10' we will run 150 jobs array in total but we will limit the 
                     number of simultaneously running tasks from this job array to 10.

  -o, OUTPUT,       Directory path for metabolic networks and the output directory by default 
                    for all other steps ( gbk ...) if no path has been 
                    defined before.                 
                        
  -b  GO_ONTOLOGY   Path for the GO-ontology -go basic file                       

  -t, TAXONFILE     Path for the lineage file. It's a tsv file commposed of two columns:
                      -the first one with the name of each genome file and 
                      -the second one the complete lineage (more details here) 
  
  -g, FASTA_ANNOT     Path for your EXISTING directory where are annotated genomes file     
                        
  -p, FASTAPROT       Path for your EXISTING directory where are proteome files 
                        
  -a ANNOTATION       Path for your EXISTING directory where are annotated tsv files 
  
optional arguments:
  -h, --help        Show this help message and exit             
                
  -k GBK              Path for the directory where you want the gbk files

  -f GFF              Path for your EXISTING directory where are gff files 

  -e PADMET_REF     Path to the padmetRef if you want to use ref for the construction of the sbml
```

##### /!\ For -k options, if you don't define the path, a folder will be created by default in the output directory you defined in option -o.

- Example of execution with default outputs:
```
sbatch 0_GeMeNet.sh -c gbk -r 0-150%10 -o output_dir/ -b Databases/Go_basic/go-basic.obo -t Lineages/lineage.tsv -g Genomes/ -p Proteomes_folder/ -a Annotation_folder/ -f gff_folder/ -e Databases/Ref_Padmet/metacyc25-5.padmet
```

- Example of execution with specific outputs directory that the user wants:
```
sbatch 0_GeMeNet.sh -c gbk -r 0-150%10 -o output_dir/ -b Databases/Go_basic/go-basic.obo -t Lineages/lineage.tsv -g Genomes/ -p Proteomes_folder/ -a Annotation_folder/ -k gbk_folder/ -f gff_folder/ -e Databases/Ref_Padmet/metacyc25-5.padmet
```


___________________________

### - `pw` mode <a name="pw"></a>

Recall: It's possible to give for each species one .tsv file but it's also possible to give a big annotation file with annotations for all species.

```
usage with default outputs:
sbatch 0_GeMeNet.sh [-h] -c CHOICE -r ARRAY [-o OUTPUT] [-k GBK] [-f GFF] [-e PADMET_REF]

                   
Mandatory arguments:
  -c, CHOICE,       Choice the first step to begin depending of your inputs data                          
                      -all : from the beginning with only the genome 
                      -em : from emapper with the genomes + proteomes files
                      -gbk : from emapper2gbk with annotated genomes + proteomes files + annotations files 
                      -pw : from mpwt step with the gbk files 
                  
  -r                 Define how many job array we want to run '--array=x-y' and we define the maximum number
                     of simultaneously running tasks from the job array by a '%' separator '--array=x-y%z'. 
                     For example: '--array=0-150%10' we will run 150 jobs array in total but we will limit the 
                     number of simultaneously running tasks from this job array to 10.

  -o, OUTPUT,       Directory path for metabolic networks          
                        
  -k GBK              Path for your EXISTING directory where are  gbk files

optional arguments:
  -h, --help        Show this help message and exit  

  -e PADMET_REF     Path to the padmetRef if you want to use ref for the construction of the sbml           
                
```

- Example of execution with default outputs:
```
sbatch 0_GeMeNet.sh -c pw -r 0-150%10 -o output_dir/ -k gbk_folder/ -f gff_folder/ -e Databases/Ref_Padmet/metacyc25-5.padmet
``` 

- Example of execution:
```
sbatch 0_GeMeNet.sh -c pw -o output_dir/ -o output_dir/ -k gbk_folder/ -f gff_folder/ -e Databases/Ref_Padmet/metacyc25-5.padmet
```


# HPC specificities: <a name="HPC"></a>

In order to facilitate the use of each user's pipeline, the #SBATCH header has been simplified as much as possible so that each user can define their own arguments.

However, some modifications need to be made in our scripts depending on the user and their data.

## Gemenet conda activation:  <a name="Gemenet"></a>

To activate the environment with the terminal, simply run this command: 
```
conda activate GeMeNet
```

In case you want to automate it and runs the environment directly with the pipeline, just uncomment these two lines line 22-23, in the script `0_Gemenet.sh` and add its own path of the gemenet environment:

```
source  <PATH>/miniconda3/etc/profile.d/conda.sh #add you path to the source of your conda env
conda activate GeMeNet
```

## Arguments #SBATCH (Arrays and Time): <a name="SBATCH"></a>

Depending on the number of data you have, each job can take more or less time. It's important to deal with this kind of issues in the script 0_Gemenet.sh by defining:
- the time in each parts "sbatch" 
- the size of your array with the number of files to process  
**/!\ be careful not every "sbatch" command need the array requirement !**

```
 RES=$(sbatch --array=1-850%10 --time=30:00:00 1_Prodigal.sh ...)
 or 
 RES2=$(sbatch --time=30:00:00 --depend=afterany:$idjob 3_emapper2gbk.sh ...)
```

You can add as many arguments as you want for each job depending on your needs (cores -c, nodes -n, --ntasks-per-node, --mem, --output, --mail-user, --mail-type ...)
_______________________________________________________
## Version:

GeMeNet version 0.1.0