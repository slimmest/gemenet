#!/bin/bash
#GeMeNet Project INRIA
#Author Muller Coralie
#Date: 21/02/2022
#Update: 25/01/2023


##################################
#    sbml generator treatement   #
##################################


############################## Execution Code ############################

### Paths for inputs and outputs
while getopts i:p: flag
do
    case "${flag}" in
        i) input_directory=${OPTARG};;
        p) path=${OPTARG};;
        :) echo "L'option $OPTARG requiert un argument"
            exit 1;;
        \?) echo "$OPTARG : option invalide"
         exit 1;;
    esac
done



padmet_with_extension="$(basename -- $padmet_file)"
padmet_without_extension=${padmet_with_extension%.*}
mkdir -p ${path}sbml/${padmet_without_extension} #Checks if the folder exists or not and creates it if it doesn't exist 
outfile=${path}sbml/${padmet_without_extension}/  #Parameter to define the path where outputfiles will be stored
echo $padmet_with_extension
echo $padmet_without_extension
echo $outfile

padmet sbmlGenerator --padmet ${padmet_file} --output ${outfile}${padmet_without_extension}.sbml -v


echo "sbatch 6_Padmet_to_sbmlGenerator.sh -i ${padmet_with_extension} -p ${path}" >> ${path}gemenet.log
echo "End the :" $(date '+%Y-%m-%d')  "at :" $( date '+%H:%M:%S')>>${path}gemenet.log
echo "GeMeNet **COMPLETED**" >>${path}gemenet.log
awk -i inplace '!seen[$0]++' ${path}gemenet.log
