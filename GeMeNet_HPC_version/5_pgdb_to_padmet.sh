#!/bin/bash
#GeMeNet Project INRIA
#Author Muller Coralie
#Date: 21/02/2022
#Update: 25/01/2023


##################################
#    PGDB to padmet treatement   #
##################################

#GeMeNet HPC

#SBATCH -J END_pipeline    #Name of the job

# source  <add you path to the source of your conda env> PATH/miniconda3/etc/profile.d/conda.sh
# conda activate GeMeNet

############################## Execution Code ############################
#Paths for input directory
while getopts i:p:r:: flag
do
    case "${flag}" in
        i) input_directory=${OPTARG};;
        p) path=${OPTARG};;
        r) path_ref=${OPTARG};;
        :) echo "L'option $OPTARG requiert un argument"
            exit 1;;
        \?) echo "$OPTARG : option invalide"
         exit 1;;
    esac
done

if [[ $path_ref == "empty" ]]
then
    current_genome=`ls ${input_directory}  | head -n $SLURM_ARRAY_TASK_ID | tail -n 1` #Get the name of the current directory genome to be process (like a loop for each subdirectory name in the main directory)

    mkdir -p ${path}padmet/${current_genome} #Checks if the folder exists or not and creates it if it doesn't exist 
    outfile=${path}padmet/${current_genome}/  #Parameter to define the path where outputfiles will be stored
    output_directory=${path}padmet/

    padmet pgdb_to_padmet --pgdb ${input_directory}${current_genome}/ --output ${outfile}${current_genome}.padmet --extract-gene -v

    echo "sbatch 5_pgdb_to_padmet.sh -i ${input_directory} -p ${path} " >> ${path}gemenet.log

    ./6_Padmet_sbmlGenerator.sh -i ${outfile}${current_genome}.padmet  -p ${path}
else

    current_genome=`ls ${input_directory}  | head -n $SLURM_ARRAY_TASK_ID | tail -n 1` #Get the name of the current directory genome to be process (like a loop for each subdirectory name in the main directory)

    mkdir -p ${path}padmet/${current_genome} #Checks if the folder exists or not and creates it if it doesn't exist 
    outfile=${path}padmet/${current_genome}/  #Parameter to define the path where outputfiles will be stored
    output_directory=${path}padmet/

    padmet pgdb_to_padmet --pgdb ${input_directory}${current_genome}/ --output ${outfile}${current_genome}.padmet --padmetRef $path_ref  --extract-gene -v



    echo "sbatch 5_pgdb_to_padmet.sh -i ${input_directory} -p ${path}  --padmetRef ${path_ref}"a >> ${path}gemenet.log

    ./6_Padmet_sbmlGenerator.sh -i ${outfile}${current_genome}.padmet  -p ${path}
fi
