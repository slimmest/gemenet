#!/bin/bash
## GeMeNet Project INRIA
## Author Muller Coralie
## Date: 14/02/2022
## Update: 25/01/2023


#####################################
#    Eggnog_mapper2gbk treatement   #
#####################################

#Eggnog_mapper2gbk is a tool starting from fasta and Eggnog-mapper annotation files, build a gbk file that is suitable for metabolic network reconstruction with Pathway Tools.
 
#SBATCH -J em2gbk   #Name of the job


############################## Execution Code ##########################################

### Path for inputs and outputs
while getopts i:g:a:b:p:t:k:f:: flag
do
    case "${flag}" in
        i) input_directory_proteome_gff=${OPTARG};;
        g) input_directory_genome=${OPTARG};;
        a) input_directory_annotation=${OPTARG};;
        b) basic=${OPTARG};;
        p) path=${OPTARG};;
        t) taxon=${OPTARG};;
        k) gbk_directory=${OPTARG};;
        f) gff_directory=${OPTARG};;
        :) echo "L'option $OPTARG requiert un argument"
            exit 1;;
        \?) echo "$OPTARG : option invalide"
         exit 1;;
    esac
done



if [[ $gff_directory == "empty" ]]
then
    mkdir -p ${gbk_directory}

    emapper2gbk genes -fn ${input_directory_genome} \
    -fp ${input_directory_proteome_gff} \
    -nf ${taxon} \
    -go ${basic} \
    -a ${input_directory_annotation} \
    -o ${gbk_directory}

    echo "sbatch 3_emapper2gbk.sh -i ${input_directory_proteome_gff} -g ${input_directory_genome} -a ${input_directory_annotation}  --one-annot-file -b ${basic} -p ${path} -t ${taxon} -k ${gbk_directory}" >> ${path}gemenet.log

    ./4_mpwt.sh -o ${gbk_directory} -p ${path}
else
    mkdir -p ${gbk_directory}

    emapper2gbk genomes -fn ${input_directory_genome} \
    -fp ${input_directory_proteome_gff} \
    -gff ${gff_directory}
    -nf ${taxon} \
    -go ${basic} \
    -a ${input_directory_annotation} \
    -o ${gbk_directory}

    echo "sbatch 3_emapper2gbk.sh -i ${input_directory_proteome_gff} -g ${input_directory_genome} -a ${input_directory_annotation} --one-annot-file -b ${basic} -p ${path} -t ${taxon} -k ${gbk_directory} -gff ${gff_directory}" >> ${path}gemenet.log

    ./4_mpwt.sh -o ${gbk_directory} -p ${path}
fi


