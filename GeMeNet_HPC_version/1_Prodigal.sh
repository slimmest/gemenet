#!/bin/bash
## GeMeNet Project INRIA
## Author Muller Coralie
## Date: 09/02/2022
## Update: 25/01/2023

############################
#    Prodigal treatement   #
############################

#Prodigal is a protein-coding gene prediction software tool for bacterial and archaeal genomes
#This script allows to lauch the script for Prodigal software and by prediction from a nucleic sequence fasta file create :
#a protein translations file / a new nucleotide with CDS sequences file / GFF format file from the fasta sequences with ONLY CDS


### Header HPC
#SBATCH -J Prodigal   #Name of the job



############################## Execution Code ##########################################


while getopts i:o:g:p:a: flag
do
    case "${flag}" in
        i) input_directory=${OPTARG};;
        o) output_directory=${OPTARG};;
        g) genome_directory=${OPTARG};;
        p) proteome_directory=${OPTARG};;
        a) annotation_directory=${OPTARG};;
        :) echo "L'option $OPTARG requiert un argument"
            exit 1;;
        \?) echo "$OPTARG : option invalide"
         exit 1;;
    esac
done

echo $input_directory
genome=`find $input_directory -type f | head -n $SLURM_ARRAY_TASK_ID | tail -n 1` #Get the name of the current file genome to be process (like a loop for each name for */fna extension in the directory)

echo $genome
genome_with_extension="$(basename -- $genome)"
echo $genome_with_extension
genome_without_extension=${genome_with_extension%.*}
extension="${genome_with_extension##*.}"


mkdir -p ${genome_directory} #Checks if the folder exists or not and creates it if it doesn't exist 
mkdir -p ${proteome_directory}
prodigal -i $genome -a  ${proteome_directory}${genome_without_extension}.faa -d ${genome_directory}${genome_without_extension}.fna ; #Command to execute the treatement 


echo "1_Prodigal.sh -i ${input_directory} -o ${output_directory} -g ${genome_directory} -p ${proteome_directory} -a ${annotation_directory} " >> ${output_directory}gemenet.log


./2_eggnog_mapper.sh -i ${proteome_directory}${genome_without_extension}.faa -f ${proteome_directory}  -o ${annotation_directory} -p ${output_directory}

