#!/bin/bash
#GeMeNet Project INRIA
#Author Muller Coralie
#Date: 17/02/2022
#Update: 25/01/2023


##################################
#          MPWT Treatment        #
##################################

#MPWT is a python package for running Pathway Tools on multiple genomes and rebuild metabolic network for each organisms.


#Paths for input directory
while getopts i:p: flag
do
    case "${flag}" in
        i) path_directory_input=${OPTARG};;
        p) path=${OPTARG};;
        :) echo "L'option $OPTARG requiert un argument"
            exit 1;;
        \?) echo "$OPTARG : option invalide"
         exit 1;;
    esac
done


mkdir -p ${path}mpwt
path_directory_output=${path}mpwt/
mkdir -p ${path}informations_mpwt

# MPWT classic 
mpwt -f ${path_directory_input} -o ${path_directory_output} --patho --md --log ${path}informations_mpwt  --taxon-file ${path_directory_input}taxon_id.tsv  -v --flat --clean

echo "sbatch 4_recon.sh -i ${path_directory_input} -p ${path}" >> ${path}gemenet.log

