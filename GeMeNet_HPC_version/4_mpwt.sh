#!/bin/bash
#GeMeNet Project INRIA
#Author Muller Coralie
#Date: 17/02/2022
#Update: 25/01/2023


#################################
#        MPWT  Job lauch        #
#################################

#Plafrim HCP

#SBATCH -J mpwt                                 #Name of the job


#Paths for input directory
while getopts o:p: flag
do
    case "${flag}" in
        o) gbk_dir=${OPTARG};;
        p) path=${OPTARG};;
        :) echo "L'option $OPTARG requiert un argument"
            exit 1;;
        \?) echo "$OPTARG : option invalide"
         exit 1;;
    esac
done


if ls ${gbk_dir}*.gbk &>/dev/null; then
    for gbkfile in ${gbk_dir}*.gbk;
    do  
        echo "$gbkfile"
        gbkfile_with_extension="$(basename -- $gbkfile)"
        echo "$gbkfile_with_extension"
        gbkfile_without_extension=${gbkfile_with_extension%.gbk}
        echo "${gbkfile_without_extension}"
        new_directory=${gbk_dir}$gbkfile_without_extension/
        echo $new_directory
        if [[ ! -d "${new_directory}" ]]
        then
            mkdir -p "${new_directory}"
            mv ${gbkfile} ${new_directory}${gbkfile_with_extension}
        fi
    done
fi


#Prepare the taxon id csv file at the root of the gbk folder
echo -e 'species\ttaxon_id' > ${gbk_dir}taxon_id.tsv


#Create the taxon id csv file with the taxon ID of the gbk
for gbkfile in ${gbk_dir}*/*.gbk ; do genome_with_extension="$(basename -- $gbkfile)" && genome_without_extension=${genome_with_extension%.*}  && gbk_id=$(grep db_xref $gbkfile | head -1 | cut -d: -f2 | cut -d\" -f1 ) && echo -e ${genome_without_extension}'\t'${gbk_id} >> ${gbk_dir}taxon_id.tsv ; done

echo "sbatch 4_mpwt.sh -o ${gbk_dir} -p ${path}" >> ${path}gemenet.log

# # run the singularity image
srun singularity exec -B external-folder/:external-folder/ path_to_the_image/mpwt_metacyc26.5.sif bash 4_recon.sh -i ${gbk_dir} -p ${path}

