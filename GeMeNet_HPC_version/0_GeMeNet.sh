#!/bin/bash
## GeMeNet Project INRIA
## Author Muller Coralie
## Date: 26/10/2022
## Update: 25/01/2023


####################################
#      GeMeNet Pipeline            #
####################################

# Header HPC  

#SBATCH -J GeMeNet   #Name of the job



############################# Module loading #############################

#### Load conda environnement:

# source  <add you path to the source of your conda env> PATH/miniconda3/etc/profile.d/conda.sh
# conda activate GeMeNet


Help()
{
   # Display Help
   echo "Add description of the script functions here."
   echo
   echo "Syntax: scriptTemplate [-c|i|o|b|t|g|p|a|k|h]"
   echo "options:"
   echo "-c     choice the mode launched all / em / gbk /mp"
   echo "                        all : from the beginning with only the genome to sbml"
   echo "                        em : from emapper with the genomes + proteomes files to sbml"
   echo "                        gbk : from emapper2gbk with genomes + proteoms + annotations files to sbml"
   echo "                        mp : from mpwt step with the gbk files to sbml"
   echo "-r     Define how many job array we want to run '--array=x-y' and we define the maximum number of simultaneously running tasks from the job array by a '%' separator '--array=x-y%z'. 
                                    For example: '--array=0-150%10' we will run 150 jobs array in total but we will limit the number of simultaneously running tasks from this job array to 10."
   echo "-i     Directory path for genome files  /!\ used only for the 'all' option"
   echo "-o     Directory path for metabolic networks and the output directory by default for all other steps if no path has been defined before."
   echo "-b     Path for the GO-ontology -go basic "
   echo "-t     Path for the lineage file. It's a tsv file commposed of two columns: the first one with the name of each genome file and the second one the complete lineage (more details in the git documentation)                   "
   echo "-g     Path for your existing folder with annotated genomes file or the path where you want the created annotated genome files. "
   echo "-p     Path for your existing folder with proteome files or the path for the folder where you want the proteome files. "
   echo "-a     Path for your existing folder with annotated tsv files or the path for your existing and unique annotated tsv file or the path for the folder where you want the created annotated tsv files or for "
   echo "-k     Path for your existing folder with gbk files or the path for the folder where you want the created annotated genome files."
   echo "-f     Path for your existing folder with gff files"
   echo "-e     Path to the padmetRef for the construction of the sbml Wwith reference from metacyc"
   echo "-h     Print this Help."
   echo
}


while getopts :hc:r:i::o:b::t::g::p::a::k::f::e:: flag
do
    case "${flag}" in
        c) choice=${OPTARG};; #choice -all, -em, -gbk, -pw
        r) array=${OPTARG};;
        i) input_directory=${OPTARG};;
        o) output_directory=${OPTARG};;
        b) basic=${OPTARG};;
        t) taxon=${OPTARG};;
        g) genome=${OPTARG};;
        p) protein=${OPTARG};;
        a) annotation=${OPTARG};;
        k) gbk=${OPTARG};;
        f) gff=${OPTARG};;
        e) ref${OPTARG};;
        h) Help # display Help
            exit;;
        :) echo "L'option $OPTARG requiert un argument"
            exit 1;;
        \?) echo "$OPTARG : option invalide"
         exit 1;;
    esac
done



############################## Execution Code ##########################################

#Checks if arguments for the choice are good
if [ -z "$choice" ] | [ -z "$output_directory" ] ; then
echo "Error: the argument for -c / -o, is not an empty argument."
echo "Please refer to the help -h or the the git documentation"
scancel $SLURM_JOB_ID
fi

#Checks if arguments for the choice are good
if [ "$choice" == "all" ] | [ "$choice" == "em" ] | [ "$choice" == "gbk" ] ; then 
    if [ -z "$basic" ] | [ -z "$taxon" ] ; then
        echo "Error: the argument for -b / -t, is not an empty argument."
        echo "Please refer to the help -h or the the git documentation"
        scancel $SLURM_JOB_ID
    fi
fi

#Checks if arguments for the choice are good
if [ -z "$array" ] ; then
echo "Error: the argument for -r , is not an empty argument."
echo "Please refer to the help -h or the the git documentation"
scancel $SLURM_JOB_ID
fi

#Checks if choice are good
if [ "$choice" != "all" ] && [ "$choice" != "em" ] && [ "$choice" != "gbk" ] && [ "$choice" != "pw" ]; then
echo "Error: the argument for -c is not valid."
echo "Please refer to the help -h or the the git documentation"
scancel $SLURM_JOB_ID
fi


# Handling error for the path of the output diretories and file extensions for each:
mkdir -p $output_directory
output_directory=${output_directory}/


if [ -z "$input_directory" ]; then
:
else
    if [ "$(ls -A $input_directory)" ]; then
        if ls ${input_directory}/*.fasta &>/dev/null || ls ${input_directory}/*.fa &>/dev/null || ls ${input_directory}/*.fna &>/dev/null || ls ${input_directory}/*.fsa &>/dev/null; then
        :
        else
        echo "The file extension does not seem to comply. Please check if the extension for the genome files is in the format : .fasta, .fna, .fsa, .fa"
        echo "Please refer to the help -h or the the git documentation"
        scancel $SLURM_JOB_ID
        fi
    fi
fi

if [ -z "$genome" ]
then
    genome_path=${output_directory}genome/
else
    genome_path=${genome}/
    mkdir -p $genome_path
    if [ "$(ls -A $genome_path)" ]; then
        if ls ${genome_path}*.fasta &>/dev/null || ls ${genome_path}*.fa &>/dev/null || ls ${genome_path}*.fna &>/dev/null || ls ${genome_path}*.fsa &>/dev/null; then
        :    
        else
        echo "The file extension does not seem to comply. Please check if the extension for the genome files is in the format : .fna"
        echo "Please refer to the help -h or the the git documentation"
        scancel $SLURM_JOB_ID
        fi
    fi
fi



if [ -z "$protein" ]
then
    proteome_path=${output_directory}proteome/
else
    proteome_path=${protein}/
    mkdir -p $proteome_path
    if [ "$(ls -A $proteome_path)" ]; then
        if ls ${proteome_path}*.faa &>/dev/null ; then
        :
        else
        echo "The file extension does not seem to comply. Please check if the extension for the proteome files is in the format : .faa"
        echo "Please refer to the help -h or the the git documentation"
        scancel $SLURM_JOB_ID
        fi
    fi
fi

if [ -z "$annotation" ]
then
    annotation_path=${output_directory}annotation/
else
    annotation_path=${annotation}
    if [[ -d "${annotation_path}" ]] ; then
        annotation_path=${annotation}/
        if [ "$(ls -A $annotation_path)" ]; then
            if ls ${annotation_path}*.tsv &>/dev/null ; then
            :  
            else
            echo "The file extension does not seem to comply. Please check if the extension for the annotation files is in the format : .tsv"
            echo "Please refer to the help -h or the the git documentation"
            scancel $SLURM_JOB_ID
            fi
        fi
    elif [[ -f "${annotation_path}" ]]; then
            if [[ $annotation_path == *.tsv ]] ; then
            :
            else
            echo "${annotation_path} is not a .tsv file "
            echo "Please refer to the help -h or the the git documentation"
            scancel $SLURM_JOB_ID
            fi
    else
        echo "${annotation_path} is not valid. It must be a .tsv file or a directory with .tsv file"
        echo "Please refer to the help -h or the the git documentation"
        scancel $SLURM_JOB_ID
        exit 1

    fi
fi

if [ -z "$gbk" ]
then
    gbk_path=${output_directory}eggnoGmapper2gbk/
else
    gbk_path=${gbk}/
    mkdir -p $gbk_path
    if [ "$(ls -A $gbk_path)" ]; then
        if find ${gbk_path} -print | grep -i '.*[.]gbk' &>/dev/null ; then
        :
        else
        echo "The file extension does not seem to comply. Please check if the extension for the genome files is in the format : .gbk"
        echo "Please refer to the help -h or the the git documentation"
        scancel $SLURM_JOB_ID
        fi
    fi
fi


if [ -z "$gff" ]
then
    gff_path="empty"
else
    gff_path=${gff}/
    mkdir -p $gff_path
    if [ "$(ls -A $gff_path)" ]; then
        if find ${gff_path} -print | grep -i '.*[.]gff' &>/dev/null ; then
        :
        else
        echo "The file extension does not seem to comply. Please check if the extension for the gff files is in the format : .gff"
        echo "Please refer to the help -h or the the git documentation"
        scancel $SLURM_JOB_ID
        fi
    fi
fi

if [ -z "$ref" ]
then
    path_ref="empty"
else
    path_ref=${ref}/
    mkdir -p $path_ref
    if [ "$(ls -A $path_ref)" ]; then
        if find ${path_ref} -print | grep -i '.*[.]padmet' &>/dev/null ; then
        :
        else
        echo "The file extension for the ref.padmet file does not seem to comply. Please check if the extension for the padmet files is in the format : .padmet"
        echo "Please refer to the help -h or the the git documentation"
        scancel $SLURM_JOB_ID
        fi
    fi
fi


########### RECAP STEPS ###############################
[ -e ${output_directory}gemenet.log ] && rm ${output_directory}gemenet.log
echo "GeMeNet version 0.1  ">> ${output_directory}gemenet.log
echo "Begin the :" $(date '+%Y-%m-%d')  "at :" $( date '+%H:%M:%S')>>${output_directory}gemenet.log
echo "Version main tools:  " >> ${output_directory}gemenet.log
echo "Prodigal V2.6.3: February, 2016  "  >> ${output_directory}gemenet.log
echo "emapper-2.1.9"  >> ${output_directory}gemenet.log
echo "emapper2gbk 0.3.1"  >> ${output_directory}gemenet.log
echo "mpwt 0.7.0  - pathwaytools 25.5"  >> ${output_directory}gemenet.log
echo "padmet 5.0.1" >> ${output_directory}gemenet.log
echo "Commands recap:" >> ${output_directory}gemenet.log



########## Main Pipeline commands ########################
input_directory=${input_directory}/

echo  '-i'  ${input_directory} '-a' ${annotation_path} '-b' ${basic} '-p' ${output_directory} '-t' ${taxon} '-k' ${gbk_path}

#Launch the all treatment: from genome file to sbml file
if [ "$choice" == "all" ]  ; then 
    RES=$(sbatch --array=${array} --time=72:00:00 1_Prodigal.sh -i ${input_directory} -o ${output_directory} -g ${genome_path} -p ${proteome_path} -a ${annotation_path}  )
    idjob=$(echo $RES | cut -d' ' -f4)
    RES2=$(sbatch --time=72:00:00 --depend=afterany:$idjob 3_emapper2gbk.sh -i ${proteome_path} -g ${genome_path} -a ${annotation_path} -b ${basic} -p ${output_directory}  -t ${taxon} -k ${gbk_path} -f ${gff_path}  )
    idjob2=$(echo $RES2 | cut -d' ' -f4)
    sbatch  --array=${array} --time=72:00:00 --depend=afterany:$idjob2 5_pgdb_to_padmet.sh -i ${output_directory}mpwt/ -p ${output_directory} -r ${path_ref}
fi

#Launch the em treatment: from genome, faa files to sbml file
if [ "$choice" == "em" ] ; then
    RES=$(sbatch --array=${array} --time=30:00:00 2_eggnog_mapper.sh -i $proteome_path -o $annotation_path -p $output_directory  )
    idjob=$(echo $RES | cut -d' ' -f4)
    RES2=$(sbatch --time=30:00:00 --depend=afterany:$idjob 3_emapper2gbk.sh -i ${proteome_path} -g ${genome_path} -a ${annotation_path} -b ${basic} -p ${output_directory} -t ${taxon} -k ${gbk_path} -f ${gff_path} )
    idjob2=$(echo $RES2 | cut -d' ' -f4)
    sbatch --array=${array} --time=30:00:00 --depend=afterany:$idjob2 5_pgdb_to_padmet.sh -i ${output_directory}mpwt/ -p ${output_directory} -r ${path_ref}
fi

#Launch the gbk treatment: from genome, faa, annotation files to sbml file
if [ "$choice" == "gbk" ] ; then
    RES=$(sbatch --time=72:00:00 3_emapper2gbk.sh -i ${proteome_path} -g ${genome_path} -a ${annotation_path} -b ${basic} -p ${output_directory} -t ${taxon} -k ${gbk_path} -f ${gff_path} )
    idjob=$(echo $RES | cut -d' ' -f4)
    sbatch --array=${array} --time=72:00:00  --depend=afterany:$idjob 5_pgdb_to_padmet.sh -i ${output_directory}mpwt/ -p ${output_directory} -r ${path_ref}
fi

#Launch the gbk treatment: from gbk file to sbml file
if [ "$choice" == "pw" ] ; then
    RES=$(sbatch  4_mpwt.sh -o ${gbk_path} -p ${output_directory} )
    idjob=$(echo $RES | cut -d' ' -f4)
    sbatch --array=${array} --time=72:00:00 --depend=afterany:$idjob 5_pgdb_to_padmet.sh -i ${output_directory}mpwt/ -p ${output_directory} -r ${path_ref}
fi
