#!/bin/bash
#GeMeNet Project INRIA
#Author Muller Coralie
#Date: 11/02/2022
#Update: 25/01/2023


#################################
#    Eggnog_mapper treatement   #
#################################

#Eggnog_mapper is a tool for fast functional annotation of novel sequences. 
#This script allows to lauch the script for Eggnog_mapper software and to generate an annotation file xxx.emapper.annotation from a proteome sequences fasta

#SBATCH -J emapper    #Name of the job


############################## Execution Code ##########################################

### Path for inputs and outputs 
while getopts f:i:o:p: flag
do
    case "${flag}" in
        f) proteome_directory=${OPTARG};;
        i) proteome=${OPTARG};;
        o) annotation_directory=${OPTARG};;
        p) path=${OPTARG};;
        :) echo "L'option $OPTARG requiert un argument"
            exit 1;;
        \?) echo "$OPTARG : option invalide"
         exit 1;;
    esac
done


if [[ -d $proteome ]];
then
    current_genome=`find $proteome -type f  | head -n $SLURM_ARRAY_TASK_ID | tail -n 1` #Get the name of the current directory genome to be process (like a loop for each subdirectory name in the main directory)
    mkdir -p ${annotation_directory} #Checks if the folder exists or not and creates it if it doesn't exist 

    genome_with_extension="$(basename -- $current_genome)"
    genome_without_extension=${genome_with_extension%.*}

    echo ${proteome}${genome_with_extension}
    #Eggnodmapper classic
    emapper.py --cpu 10 -i ${proteome}${genome_with_extension} -o ${genome_without_extension} --output_dir ${annotation_directory}  -m diamond --override;   #Command to execute the treatement 
    mv ${annotation_directory}${genome_without_extension}.emapper.annotations ${annotation_directory}${genome_without_extension}.tsv
    rm ${annotation_directory}${genome_without_extension}.emapper.hits
    rm ${annotation_directory}${genome_without_extension}.emapper.seed_orthologs


    echo "2_eggnog_mapper.sh -i $proteome -o $annotation_directory -p $path " >> ${path}gemenet.log
else
    mkdir -p ${annotation_directory} #Checks if the folder exists or not and creates it if it doesn't exist 
    proteome_with_extension="$(basename -- $proteome)"
    proteome_without_extension=${proteome_with_extension%.*}

    echo ${proteome}${proteome_with_extension}
    #Eggnodmapper classic
    emapper.py --cpu 10 -i ${proteome} -o ${proteome_without_extension} --output_dir ${annotation_directory}  -m diamond --override;   #Command to execute the treatement 
    mv ${annotation_directory}${proteome_without_extension}.emapper.annotations ${annotation_directory}${proteome_without_extension}.tsv
    rm ${annotation_directory}${proteome_without_extension}.emapper.hits
    rm ${annotation_directory}${proteome_without_extension}.emapper.seed_orthologs


    echo "2_eggnog_mapper.sh -i $proteome_directory -o $annotation_directory -p $path " >> ${path}gemenet.log
fi
